//
//  ASWKeysSDK.swift
//  Ausweis.ioSDK
//
//  Created by admin1 on 4/26/18.
//

import Foundation


final public class ASWKeysLocksSDK {
	
	private var keysManager = KeysAPIManager()
	private var locksManager = LockAPIManager()
	
	private let dispatchGroup = DispatchGroup()
	
	typealias Params = Dictionary<String,String>
	
	public init() {
		
	}
	
	public func getAllKeys(_ completion: @escaping (_ keys: [Key]?) -> (), failure: @escaping (_ error: ResponseError?) -> ()) {
		keysManager.getAllKeys { (keys, errorResult, errorBody) in
			if let keys = keys {
				completion(keys)
			} else if let errorBody = errorBody {
				failure(errorBody)
			} else if let errorResult = errorResult {
				if let failureError = errorResult.failureValue() {
					let responseError = ResponseError(statusCode: failureError.code, error: failureError.message)
					failure(responseError)
				}
			}
		}
	}
	
	public func openLockWithKey(_ keyID: Int, completion: @escaping (_ success: Bool) -> (),  failure: @escaping (_ error: ResponseError?) -> ()) {
		keysManager.openLockWithKey(keyID) { (success, errorResult, errorBody) in
			if success {
				completion(success)
			} else if let errorBody = errorBody {
				failure(errorBody)
			} else if let errorResult = errorResult {
				if let failureError = errorResult.failureValue() {
					let responseError = ResponseError(statusCode: failureError.code, error: failureError.message)
					failure(responseError)
				}
			}
		}
	}
	
	public func openOfflineLockWithKey(_ key: Key,  completion: @escaping (_ success: Bool) -> (),  failure: @escaping (_ error: ResponseError?) -> ()) {
		
		var config: Params!
		
		if let secretConfig = key.secretConfig {
			config = ["command":"config", "config":secretConfig.config, "iv":secretConfig.secretIV, "timestamp": "\(Date().timeIntervalSince1970)"]
		}
		
		let open : Params = ["command":"open", "config":key.secretKey!.config, "iv":key.secretKey!.secretIV, "timestamp": "\(Int(Date().timeIntervalSince1970))"]
		
		HotspotManager().connect(key.lockInfo.wifiSettings!.lockWifiSSID!, password: key.lockInfo.wifiSettings!.lockWifiPassword!) { (success, error) in
			if success {
				if let config = config {
					self.startOfflineLockOpening(config: config, open: open)
				} else {
					self.startLocalLockOpening(open: open)
				}
			} else {
				print(error!)
			}
		}
	
	}
	
	public func getLockDetails(_ lockID: Int, completion: @escaping (_ lock: LockDetailsResponse) -> (),  failure: @escaping (_ error: ResponseError?) -> ()) {
		locksManager.getLockDetails(lockID) { (lockDetails, errorResult, errorBody) in
			if let lock = lockDetails {
				completion(lock)
			} else if let errorBody = errorBody {
				failure(errorBody)
			} else if let errorResult = errorResult {
				if let failureError = errorResult.failureValue() {
					let responseError = ResponseError(statusCode: failureError.code, error: failureError.message)
					failure(responseError)
				}
			}
		}
	}
	
	public func getLock(_ lockID: Int, completion: @escaping (_ lock: Lock) -> (),  failure: @escaping (_ error: ResponseError?) -> ()) {
		locksManager.getLock(lockID) { (lockResponse, errorResult, errorBody) in
			if let lock = lockResponse {
				completion(lock)
			} else if let errorBody = errorBody {
				failure(errorBody)
			} else if let errorResult = errorResult {
				if let failureError = errorResult.failureValue() {
					let responseError = ResponseError(statusCode: failureError.code, error: failureError.message)
					failure(responseError)
				}
			}
		}
	}

	
	public func updateLock(_ lock: Lock, completion: @escaping (_ lock: Lock) -> (),  failure: @escaping (_ error: ResponseError?) -> ()) {
		locksManager.updateLock(lock) { (lock, errorResult, errorBody) in
			if let lock = lock {
				completion(lock)
			} else if let errorBody = errorBody {
				failure(errorBody)
			} else if let errorResult = errorResult {
				if let failureError = errorResult.failureValue() {
					let responseError = ResponseError(statusCode: failureError.code, error: failureError.message)
					failure(responseError)
				}
			}
		}
	}
	
	public func getLockHistory(_ lockID: Int,  offset:Int, limit: Int = 20, completion: @escaping (_ lockHistory: [LockHistory]) -> (),  failure: @escaping (_ error: ResponseError?) -> ()) {
		locksManager.getLockHistory(lockID, offset: offset, limit: limit) { (lockHistoryResponse, errorResult, errorBody) in
			if let lockHistory = lockHistoryResponse {
				completion(lockHistory.results)
			} else if let errorBody = errorBody {
				failure(errorBody)
			} else if let errorResult = errorResult {
				if let failureError = errorResult.failureValue() {
					let responseError = ResponseError(statusCode: failureError.code, error: failureError.message)
					failure(responseError)
				}
			}
		}
	}
	
	public func registerLock(withData data: LockRegister, completion: @escaping (_ lockActivation: LockActivation?) -> (),  failure: @escaping (_ error: ResponseError?) -> ()) {
		
		
		locksManager.registerLock(data) { (lockActivationResponse, errorResult, errorBody) in
			if let lockActivation = lockActivationResponse {
				completion(lockActivation)

				HotspotManager().connect(lockActivation.wifi.lockWifiSSID ?? "", password: lockActivation.wifi.lockWifiPassword ?? "") { (success, error) in
					if success {
						self.startActivating(config: lockActivation)
					} else {
						print(error!)
					}
				}
				
			} else if let errorBody = errorBody {
				failure(errorBody)
			} else if let errorResult = errorResult {
				if let failureError = errorResult.failureValue() {
					let responseError = ResponseError(statusCode: failureError.code, error: failureError.message)
					failure(responseError)
				}
			}
		}
	}
	
	
	public func shareLock(_ key: KeyShare!) {
		if let keyShare = key {
			try! keysManager.shareKeyForLock(keyShare) { (success, result, error) in
				
			}
		}
		
		
	}
	

	
	public func updateWifi(withKey key: Key) {
		
		let params: Params = ["command":"reset_wifi", "config":key.secretKey!.config, "iv":key.secretKey!.secretIV, "timestamp": "\(Int(Date().timeIntervalSince1970))","password_router": "impthepice16", "ssid_router": "\(key.lockInfo.wifiSettings!.homeWifiSSID!)"]
		
		HotspotManager().connect(key.lockInfo.wifiSettings!.lockWifiSSID!, password: key.lockInfo.wifiSettings!.lockWifiPassword!) { (success, error) in
			if success {
				self.updateLockWifiSettings(params: params)
			} else {
				print(error!)
			}
		}
	}
	
	
}

private extension ASWKeysLocksSDK {
	
	func startOfflineLockOpening(config: Params, open:Params) {
		let jsonData = try! JSONEncoder().encode(config)
		let jsonData2 = try! JSONEncoder().encode(open)
		
		let dataSend = SocketManager.shared.connect().sendMessage
		dataSend(jsonData)
			.flatMap { (response) -> Future<String>! in
				print("First response \(Date())")
				if response.contains("ACK") {
					return dataSend(jsonData2)
				} else {
					print("First wrong response \(response)")
					return nil
				}
			}?.subscribe(onNext: { (response) in
				print("second response \(Date())")
				if response.contains("ACK") {
					print("VERY COOL RESULT YO YO")
				} else {
					print("Second wrong response \(response)")
				}
			}, onError: { (error) in
				print(error)
			})
	}
	
	func startLocalLockOpening(open:Params) {
		let jsonData = try! JSONEncoder().encode(open)
		
		let dataSend = SocketManager.shared.connect().sendMessage
		dataSend(jsonData).subscribe(onNext: { (response) in
				print("response \(Date())")
				if response.contains("ACK") {
					print("VERY COOL RESULT YO YO")
				} else {
					print("Second wrong response \(response)")
				}
			}, onError: { (error) in
				print(error)
			})
	}
	
	func startActivating(config: LockActivation) {
		let jsonData = try! JSONEncoder().encode(config)
		
		let dataSend = SocketManager.shared.connect().sendMessage
		dataSend(jsonData)
			.subscribe(onNext: { (response) in
				print("second response \(Date())")
				if response.contains("ACK") {
					print("ACTIVATED")
				} else {
					print("Wrong response \(response)")
				}
			}, onError: { (error) in
				print(error)
			})
	}
	
	
	func updateLockWifiSettings(params:Params) {
		let jsonData = try! JSONEncoder().encode(params)
		
		let dataSend = SocketManager.shared.connect().sendMessage
		dataSend(jsonData)
			.subscribe(onNext: { (response) in
				print("second response \(Date())")
				if response.contains("ACK") {
					print("WIFI SETTINGS CHANGED")
				} else {
					print("Wrong response \(response)")
				}
			}, onError: { (error) in
				print(error)
			})
	}
	
	
}


