//
//  KeysAPIManager.swift
//  Ausweis.ioSDK
//
//  Created by admin1 on 4/26/18.
//

import Foundation


struct KeysAPIManager: NetworkManager {
	
	
	
	internal var urlConfig: URLConfiguration {
		return URLConfiguration()
	}
	
	private let router = Router<KeysAPI>()
	
	init () {
	}
	
	public func getAllKeys(_ completion: @escaping (_ keys: [Key]?, _ errorResult: Result<String>?, _ errorBody: ResponseError? ) -> ()) {
		
		self.router.request(.getAllKeys(accessToken: AccessManager().accessToken() )) { (data, response, error) in
			if error != nil {
				completion(nil, Result.failure(NetworkError.noConnection.rawValue, statusCode: -999), nil)
			}
			
			guard let httpResponse = response as? HTTPURLResponse else {
				completion(nil, Result.failure(NetworkError.noConnection.rawValue, statusCode: -995), nil)
				return
			}
			
			self.handleResponse(responseType: [Key].self, data, httpResponse, completion)
		}
	}
	
	public func openLockWithKey(_ keyID: Int, completion: @escaping (_ success: Bool, _ errorResult: Result<String>?, _ errorBody: ResponseError? ) -> ()) {
		self.router.request( .openKeyWithLock(accessToken: AccessManager().accessToken(), lockID: keyID) ) { (data, response, error) in
			if error != nil {
				completion(false, Result.failure(NetworkError.noConnection.rawValue, statusCode: -999), nil)
				return
			}
			guard let httpResponse = response as? HTTPURLResponse else {
				completion(false, Result.failure(NetworkError.noConnection.rawValue, statusCode: -995), nil)
				return
			}
			
			
			
			if httpResponse.statusCode == 200 {
				completion(true,nil,nil)
			} else {
				self.handleError(responseData: data, httpResponse, { (result, error) in
					completion(false, result, error)
				})
			}
			
		}
	}
	
	
	public func shareKeyForLock(_ keyShare: KeyShare, completion: @escaping (_ participantKey: Participant?, _ errorResult: Result<String>?, _ errorBody: ResponseError? ) -> ()) throws {
		
		let shareData = try JSONEncoder().encode(keyShare)
		
		self.router.request(.shareKey(accessToken: AccessManager().accessToken(), shareData: shareData)) { (data, response, error) in
			if error != nil {
				completion(nil, Result.failure(NetworkError.noConnection.rawValue, statusCode: -999), nil)
				return
			}
			guard let httpResponse = response as? HTTPURLResponse else {
				completion(nil, Result.failure(NetworkError.noConnection.rawValue, statusCode: -995), nil)
				return
			}
			
			self.handleResponse(responseType: Participant.self, data, httpResponse, completion)
			
//			switch httpResponse.statusCode {
//			case 200...201:
//				completion(true,nil,nil)
//			default :
//				self.handleError(responseData: data, httpResponse, { (result, error) in
//					completion(false, result, error)
//				})
//			}
		}
		
		
	}
	
	
}

extension KeysAPIManager {
	
	func handleNetworkResponse(_ response: HTTPURLResponse) -> Result<String> {
		
		switch  response.statusCode {
		case 400			: return .failureResponse
		case 200...299: return .success
		case 401			: return .failure(NetworkResponse.authenticationError.rawValue, statusCode: response.statusCode)
		case 403			: return .failureResponse
		case 500...599: return .failure(NetworkResponse.badRequest.rawValue, statusCode: response.statusCode)
		default:
			return .failure(NetworkResponse.failed.rawValue, statusCode: response.statusCode)
		}
	}
	
	
	func handleResponse<T>(responseType: T.Type ,_ responseData: Data?, _ response: HTTPURLResponse, _ completion: @escaping (T?, Result<String>?, ResponseError?) -> ()) where T : Decodable {
		
		let result = self.handleNetworkResponse(response)
		switch result {
		case .success :
			guard let responseData = responseData else {
				completion(nil, Result.failure(NetworkResponse.noData.rawValue, statusCode: -998), nil)
				return
			}
			
			do {
				let apiResponse = try JSONDecoder().decode(T.self, from: responseData)
				completion(apiResponse, nil, nil)
			} catch {
				completion(nil, Result.failure(NetworkResponse.unableToDecode.rawValue, statusCode: -997), nil)
			}
		case .failureResponse:
			handleError(responseData: responseData, response, { (result, error) in
				completion(nil, result,error)
			})
		case .failure(_, _) :
			completion(nil, result, nil)
		}
	}
	
}
