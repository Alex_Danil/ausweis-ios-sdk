//
//  KeysResponse.swift
//  Ausweis.ioSDK
//
//  Created by admin1 on 5/15/18.
//

import Foundation

struct KeySecret : Mappable {
	var secretIV: String
	var config: String
	
	enum CodingKeys: String, CodingKey {
		case iv
		case config
	}
	
	public init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		
		secretIV = try container.decode(String.self, forKey: .iv)
		config = try container.decode(String.self, forKey: .config)
	}

}

public struct KeysResponse : Mappable {
	var keys: [Key]?
	
	public init(from decoder: Decoder) throws {
		
	}
	
}

public struct Key : Mappable {
	public var ID: Int
//	var modified: Date
//	var limitationDate: Date!
	public var isLimited: Bool
	public var isActive: Bool
	public var lockInfo: LockResponse
	public var keyType: String?
	public var isOneTimeKey: Bool
	internal var secretKey: KeySecret?
	internal var secretConfig: KeySecret?
	public var isScheduled: Bool
//	var schedule: Array<Any>
//	var scheduleRange: Any?
	public var isValid: Bool
//	var createdDate: Date
	
	enum CodingKeys: String, CodingKey {
		case id
		case modified
		case limitation_time
		case time_limited
		case active
		case lock
		case lock_info
		case key_type
		case one_time
		case secret_key
		case with_schedule
		case schedule
		case secret_config
		case valid
		case created
	}

	public init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		
		ID = try container.decode(Int.self, forKey: .id)
		let modifiedDateString = try container.decode(String.self, forKey: .modified)
		let limitationDateString = try container.decodeIfPresent(String.self, forKey: .limitation_time)
		isLimited = try container.decode(Bool.self, forKey: .time_limited)
		isActive = try container.decode(Bool.self, forKey: .active)
		lockInfo = try container.decode(LockResponse.self, forKey: .lock_info)
		keyType = try container.decodeIfPresent(String.self, forKey: .key_type)
		isOneTimeKey = try container.decode(Bool.self, forKey: .one_time)
		secretConfig = try container.decodeIfPresent(KeySecret.self, forKey: .secret_config)
		secretKey = try container.decodeIfPresent(KeySecret.self, forKey: .secret_key)

		isScheduled = try container.decode(Bool.self, forKey: .with_schedule)
//		schedule = try container.decode(Array<Any>.self, forKey: .schedule)
//		scheduleRange = try container.decode(Any.self, forKey: .range)
		isValid = try container.decode(Bool.self, forKey: .valid)
		let createdDateString = try container.decode(String.self, forKey: .created)
		
//		modified = .aswDateFormatter(modifiedDateString)
//		(modifiedDateString) ?? Date()
//		limitationDate = formatter.date(from: limitationDateString ?? "") ?? Date()
//		createdDate = .aswDateFormatter(createdDateString)
		
	}


	
	
}
