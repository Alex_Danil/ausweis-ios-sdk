//
//  KeyShare.swift
//  Ausweis.ioSDK
//
//  Created by admin1 on 6/4/18.
//

import Foundation


public class KeyShare: Mappable, Encodable {
	var isLimited: Bool?
	var isTimeLimited: Bool?
	var name: String?
	var schedule: [KeySchedule]?
	var lock: Int
	var isOneTime: Bool?
	var range: KeyRange?
	var isActivityEnabled: Bool?
	var isActive: Bool?
	var isWithSchedule: Bool?
	var isLinkInvite: Bool?
	var email: String?
	
	enum CodingKeys: String, CodingKey{
		case limited
		case time_limited
		case name
		case schedule
		case lock
		case one_time
		case range
		case activity
		case active
		case with_schedule
		case email
		case link_invite

	}

	
	public required init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		
		isLimited = try container.decodeIfPresent(Bool.self, forKey: .limited)
		isTimeLimited = try container.decodeIfPresent(Bool.self, forKey: .time_limited)
		name = try container.decodeIfPresent(String.self, forKey: .name)
		schedule = try container.decodeIfPresent([KeySchedule].self, forKey: .schedule)
		lock = try container.decode(Int.self, forKey: .lock)
		isOneTime = try container.decodeIfPresent(Bool.self, forKey: .one_time)
		range = try container.decodeIfPresent(KeyRange.self, forKey: .range)
		isActivityEnabled = try container.decodeIfPresent(Bool.self, forKey: .activity)
		isActive = try container.decodeIfPresent(Bool.self, forKey: .active)
		isWithSchedule = try container.decodeIfPresent(Bool.self, forKey: .with_schedule)
		email = try container.decodeIfPresent(String.self, forKey: .email)
		isLinkInvite = try container.decode(Bool.self, forKey: .link_invite)

	}
	
	
	
	
	
	public func encode(to encoder: Encoder) throws {
		var container = encoder.container(keyedBy: CodingKeys.self)

		try container.encodeIfPresent(isLimited, forKey: .limited)
		try container.encodeIfPresent(isTimeLimited, forKey: .time_limited)
		try container.encodeIfPresent(name, forKey: .name)
		try container.encodeIfPresent(schedule, forKey: .schedule)
		try container.encode(lock, forKey: .lock)
		try container.encodeIfPresent(isOneTime, forKey: .one_time)
		try container.encodeIfPresent(range, forKey: .range)
		try container.encodeIfPresent(isActivityEnabled, forKey: .activity)
		try container.encodeIfPresent(isActive, forKey: .active)
		try container.encodeIfPresent(isWithSchedule, forKey: .with_schedule)
		try container.encodeIfPresent(email, forKey: .email)
		try container.encodeIfPresent(isLinkInvite, forKey: .link_invite)
		
	}
	
}


class KeyRange: Mappable, Encodable {
	let upper: Date!
	let lower: Date!
	
	enum CodingKeys: String, CodingKey{
		case upper
		case lower
	}
	
	public required init(from decoder: Decoder) throws {
		var container = try decoder.container(keyedBy: CodingKeys.self)
		
		let formatter = Date.externalDateFormatter()
		
		let upperString = try container.decode(String.self, forKey: .upper)
		let lowerString = try container.decode(String.self, forKey: .lower)
		
		upper = formatter.date(from: upperString)
		lower = formatter.date(from: lowerString)

	}
	
	public func encode(to encoder: Encoder) throws {
		var container = encoder.container(keyedBy: CodingKeys.self)
		let formatter = Date.externalDateFormatter()
		do {
			if let upperDate = upper, let lowerDate = lower {
				let upperString = formatter.string(from: upperDate)
				let lowerString = formatter.string(from: lowerDate)
				try container.encodeIfPresent(upperString, forKey: .upper)
				try container.encodeIfPresent(lowerString, forKey: .lower)
			}
		} catch {
			print("Encoding error : \(error)")
		}
	}
	
}


class KeySchedule : Mappable, Encodable {
	var days: Array<Int>
	var endTime: String
	var startTime: String
	
	enum CodingKeys: String, CodingKey{
		case days
		case start
		case end
	}
	
	
	public required init(from decoder: Decoder) throws {
		var container = try decoder.container(keyedBy: CodingKeys.self)
		
		days = try container.decode([Int].self, forKey: .days)
		startTime = try container.decode(String.self, forKey: .start)
		endTime = try container.decode(String.self, forKey: .end)
	}
	
	public func encode(to encoder: Encoder) throws {
		var container = encoder.container(keyedBy: CodingKeys.self)
		do {
			try container.encodeIfPresent(days, forKey: .days)
			try container.encodeIfPresent(startTime, forKey: .start)
			try container.encodeIfPresent(endTime, forKey: .end)
		} catch {
			print("Encoding error : \(error)")
		}
	}
	
}




