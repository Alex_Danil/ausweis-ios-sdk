//
// KeysAPI.swift
// Pods
//
// Created by admin1 on 4/25/18.
//
//

import Foundation

enum KeysAPI {
	case getAllKeys( accessToken: String)
	case openKeyWithLock( accessToken: String, lockID: Int)
	case shareKey( accessToken: String, shareData: Data)
}

extension KeysAPI : EndpointType {
	
	var headers: HTTPHeaders? {
		switch self {
		case .getAllKeys(let accessToken), .openKeyWithLock(let accessToken, _), .shareKey(let accessToken, _):
			return ["Authorization": "JWT \(accessToken)"]
		}
	}
	
	var baseURL: URL {
		return URL(string: URLConfiguration.Environment.Develop.rawValue)!
	}
	
	var path: String {
		switch self {
		case .getAllKeys:
			return "/api/v1/keys/all/"
		case .openKeyWithLock(_ , let lockID):
			return "/api/v1/keys/\(lockID)/open_lock/"
		case .shareKey:
			return "/api/v1/keys/create-key/"
		}
	}
	
	var httpMethod: HTTPMethod {
		switch self {
		case .getAllKeys:
			return .get
		case .openKeyWithLock, .shareKey:
			return .post
		}
	}
	
	var task: HTTPTask {
		switch self {
		case .getAllKeys, .openKeyWithLock:
				return .requestParametersAndHeaders(bodyParameters: nil, urlParameters: nil, additionalHeaders: headers)
		case .shareKey(_ , let body):
			return .requestBody(bodyParameters: body, additionalHeaders: headers)
		}
	}
	
}



