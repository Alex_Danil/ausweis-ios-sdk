//
//  Participant.swift
//  Ausweis.ioSDK
//
//  Created by admin1 on 5/22/18.
//

import Foundation

public enum ParticipantKeyType: String {
	case link = "link_invite"
}

open class Participant: Mappable {
	var createdDate: Date?
	var ID: Int
	var url: String?
	var isLimited: Bool
	var shareUrl: String?
//	var range: Any?
	var keyType: ParticipantKeyType?
	var photo: String?
	var phone: String?
	var isActivity: Bool
	var email: String?
	var linkInvite: String?
	var accessType: LockAccessType?
	var isTimeLimited : Bool
	var isScheduled: Bool
	var isOneTime: Bool
	
	enum CodingKeys: String, CodingKey {
		case created
		case id
		case url
		case limited
		case range
		case key_type
		case photo
		case phone
		case activity
		case email
		case invite_link
		case access_type
		case time_limited
		case with_schedule
		case one_time
		case share_url
	}
	
	
	public required init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		
		ID = try container.decode(Int.self, forKey: .id)
		url = try container.decodeIfPresent(String.self, forKey: .url)
		isLimited = try container.decode(Bool.self, forKey: .limited)
		
		if let keyTypeString = try container.decodeIfPresent(String.self, forKey: .key_type) {
			keyType = ParticipantKeyType(rawValue: keyTypeString)
		}
	
		photo = try container.decodeIfPresent(String.self, forKey: .photo)
		phone = try container.decodeIfPresent(String.self, forKey: .phone)
		
		isActivity = try container.decode(Bool.self, forKey: .activity)
		email = try container.decodeIfPresent(String.self, forKey: .email)
		linkInvite = try container.decodeIfPresent(String.self, forKey: .invite_link)
		
		if let accessTypeString = try container.decodeIfPresent(String.self, forKey: .access_type) {
			accessType = LockAccessType(rawValue: accessTypeString)
		}
		
		isTimeLimited = try container.decode(Bool.self, forKey: .time_limited)
		isScheduled = try container.decode(Bool.self, forKey: .with_schedule)
		isOneTime = try container.decode(Bool.self, forKey: .one_time)
		
		if let createdDateString = try container.decodeIfPresent(String.self, forKey: .created) {
				createdDate = Date.aswDateFormatter(createdDateString)
		}
		
		shareUrl = try container.decode(String.self, forKey: .share_url)
	}
	
}
