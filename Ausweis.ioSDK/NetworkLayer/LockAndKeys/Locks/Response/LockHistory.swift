//
//  LockHistory.swift
//  Ausweis.ioSDK
//
//  Created by admin1 on 5/30/18.
//

import Foundation

class LockHistoryResponse : Mappable {
	open let results: [LockHistory]
	
	enum CodingKeys: String, CodingKey {
		case results
	}
	
	required init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		
		results = try container.decode([LockHistory].self, forKey: .results)
	}
}

open class LockHistory : Mappable {
	
	let ID : Int
	let lockID: Int
	let lockName: String
	let keyID: Int!
	let userID: Int!
	let userEmail: String!
	let isByButton: Bool!
	let isByCard: Bool!
	let isLocalMode: Bool!
	
	let dateString: String!
	let userInfo: LockHistoryUser!
	let userName: String!
	let openedBy: String!
	let displayName: String
	
	enum CodingKeys: String, CodingKey {
		case id
		case lock
		case lock_name
		case key
		case user
		case user_email
		case by_button
		case by_card
		case local_mode
		case date
		case user_info
		case user_name
		case opened_by
		case name_to_display
	}
	
	required public init(from decoder: Decoder) throws {
		let container = try  decoder.container(keyedBy: CodingKeys.self)
		
		ID = try container.decode(Int.self, forKey: .id)
		lockID = try container.decode(Int.self, forKey: .lock)
		lockName = try container.decode(String.self, forKey: .lock_name)
		keyID = try container.decodeIfPresent(Int.self, forKey: .key)
		userID = try container.decodeIfPresent(Int.self, forKey: .user)
		userEmail = try container.decodeIfPresent(String.self, forKey: .user_email)
		isByButton = try container.decodeIfPresent(Bool.self, forKey: .by_button)
		isByCard = try container.decodeIfPresent(Bool.self, forKey: .by_card)
		isLocalMode = try container.decodeIfPresent(Bool.self, forKey: .local_mode)
		dateString = try container.decodeIfPresent(String.self, forKey: .date)
		
		userInfo = try container.decodeIfPresent(LockHistoryUser.self, forKey: .user_info)
		userName = try container.decodeIfPresent(String.self, forKey: .user_name)
		openedBy = try container.decodeIfPresent(String.self, forKey: .opened_by)
		displayName = try container.decode(String.self, forKey: .name_to_display)
	}
	
}

open class LockHistoryUser : Mappable {
	let ID: Int
	let email: String
	let name: String
	let phone: String?
	let photo: String
	
	enum CodingKeys: String, CodingKey {
		case id
		case email
		case name
		case phone
		case photo
	}
	
	required public init(from decoder: Decoder) throws {
		let container = try  decoder.container(keyedBy: CodingKeys.self)

		ID = try container.decode(Int.self, forKey: .id)
		email = try container.decode(String.self, forKey: .email)
		name = try container.decode(String.self, forKey: .name)
		phone = try container.decodeIfPresent(String.self, forKey: .phone)
		photo = try container.decode(String.self, forKey: .photo)
	}
	
}
