//
//  LockActivation.swift
//  Ausweis.ioSDK
//
//  Created by admin1 on 6/1/18.
//

import Foundation

open class LockActivation : Mappable, Encodable {
	
	let aesKey: String
	var command: String = "activate"
	let microsoftKey: String
	let mode: LockMode
	var passwordRouter: String?
	var ssidRouter: String?
	let subtype: LockSubType
	var timeZone: Double?
	var timestamp: Int?
	var timing: Int?
	let type: LockType
	let wifi: LockWifi
	
	enum CodingKeys : String, CodingKey {
		case aes_key
		case command
		case microsoft_key
		case mode
		case password_router
		case ssid_router
		case subtype
		case time_zone
		case timestamp
		case timing
		case type
		
		case wifi
		
	}
	
	
	public required init(from decoder: Decoder) throws {
		var container = try decoder.container(keyedBy: CodingKeys.self)
		
		aesKey = try container.decode(String.self, forKey: .aes_key)
		microsoftKey = try container.decode(String.self, forKey: .microsoft_key)
		let rawMode = try container.decode(String.self, forKey: .mode)
		mode = LockMode(rawValue: rawMode)!
		let rawSubtype = try container.decode(String.self, forKey: .subtype)
		subtype = LockSubType(rawValue: rawSubtype)!
		let rawType = try container.decode(String.self, forKey: .type)
		type = LockType(rawValue: rawType)!
		wifi = try container.decode(LockWifi.self, forKey: .wifi)
	}
	
	public func encode(to encoder: Encoder) throws {
		
		do {
			var container = encoder.container(keyedBy: CodingKeys.self)
			
			try container.encode(aesKey, forKey: .aes_key)
			try container.encode(command, forKey: .command)
			try container.encode(microsoftKey, forKey: .microsoft_key)
			try container.encode(mode.rawValue, forKey: .mode)
			try container.encode(passwordRouter, forKey: .password_router)
			try container.encode(ssidRouter, forKey: .ssid_router)
			try container.encode(subtype.rawValue, forKey: .subtype)
			try container.encode(timeZone, forKey: .time_zone)
			try container.encode(timestamp, forKey: .timestamp)
			try container.encode(timing, forKey: .timing)
			try container.encode(type.rawValue, forKey: .type)
			try container.encodeIfPresent(wifi.lockWifiSSID, forKey: .ssid_router)
			try container.encodeIfPresent(wifi.lockWifiPassword, forKey: .password_router)
		} catch {
			print("Encoding error: \(error)")
		}
	}
	
}
