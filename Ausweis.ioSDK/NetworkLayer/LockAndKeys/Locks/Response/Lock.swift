//
//  Lock.swift
//  Ausweis.ioSDK
//
//  Created by admin1 on 6/5/18.
//

import Foundation
import CoreLocation

public class Lock: Mappable, Encodable {
	var ID: Int
	var isActivatedByPhone: Bool
	var location: CLLocation?
	var name: String
	var isActivityNotification: Bool
	var wifi : LockWifi?
	var isActivated: Bool
	var timeZone: String
	
	var isAllowGuestSetAlarm: Bool
	var subtype: LockSubType!
	var mac: String
	var version: Int
	var isButtonDisabled: Bool
	var mode: LockMode!
	public var address: String
	var timing: Int
	var subscriptionCode: String?
	var type: LockType!
	var isAutoHomeWifiConnect: Bool
	var isRemoteOpening: Bool
	
	
	public enum EncodingKeys: String, CodingKey {
		case id
		case activated_by_phone
		case lat
		case long
		case name
		case activity_notification
		case wifi
		case activated
		case time_zone
		case allow_guest_set_alarm
		case subtype
		case mac
		case version
		case button_disabled
		case mode
		case address
		case timing
		case subscription_code
		case type
		case auto_home_wifi_connect
		case remote_opening
	}
	
	
	public required init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: EncodingKeys.self)
		
		ID = try container.decode(Int.self, forKey: .id)
		isActivatedByPhone = try container.decode(Bool.self, forKey: .activated_by_phone)
		
		if 	let lat = try container.decodeIfPresent(Double.self, forKey: .lat),
			let lon = try container.decodeIfPresent(Double.self, forKey: .long) {
			location = CLLocation(latitude: lat, longitude: lon)
		}
		
		name = try container.decode(String.self, forKey: .name)
		isActivityNotification = try container.decode(Bool.self, forKey: .activity_notification)
		wifi = try container.decodeIfPresent(LockWifi.self, forKey: .wifi)
		isActivated = try container.decode(Bool.self, forKey: .activated)
		timeZone = try container.decode(String.self, forKey: .time_zone)
		isAllowGuestSetAlarm = try container.decode(Bool.self, forKey: .allow_guest_set_alarm)
		
		if let subtypeString = try? container.decode(String.self, forKey: .subtype) {
			subtype = LockSubType(rawValue: subtypeString) ?? .high
		}
		
		mac = try container.decode(String.self, forKey: .mac)
		version = try container.decode(Int.self, forKey: .version)
		isButtonDisabled = try container.decode(Bool.self, forKey: .button_disabled)
		
		if let modeString = try? container.decode(String.self, forKey: .mode) {
			mode = LockMode(rawValue: modeString) ?? .offline
		}
		
		address = try container.decode(String.self, forKey: .address)
		timing = try container.decode(Int.self, forKey: .timing)
		subscriptionCode = try container.decodeIfPresent(String.self, forKey: .subscription_code)
		
		if let typeString = try? container.decode(String.self, forKey: .type) {
			type = LockType(rawValue: typeString) ?? .accessControl
		}
		
		isAutoHomeWifiConnect = try container.decode(Bool.self, forKey: .auto_home_wifi_connect)
		isRemoteOpening = try container.decode(Bool.self, forKey: .remote_opening)

	}

	public func encode(to encoder: Encoder) throws {
		do {
			var container = encoder.container(keyedBy: EncodingKeys.self)
			try container.encodeIfPresent(timing, forKey: .timing)
			try container.encodeIfPresent(address, forKey: .address)
			try container.encode(mac, forKey: .mac)
			try container.encode(mode.rawValue, forKey: .mode)
			try container.encodeIfPresent(timeZone, forKey: .time_zone)
			try container.encodeIfPresent(subtype.rawValue, forKey: .subtype)
			try container.encodeIfPresent(type.rawValue, forKey: .type)
			
			try container.encodeIfPresent(location?.coordinate.latitude, forKey: .lat)
			try container.encodeIfPresent(location?.coordinate.longitude, forKey: .long)
			try container.encode(name, forKey: .name)
			try container.encodeIfPresent(wifi, forKey: .wifi)
			
			try container.encodeIfPresent(isActivityNotification, forKey: .activity_notification)
			try container.encodeIfPresent(isAllowGuestSetAlarm, forKey: .allow_guest_set_alarm)
			try container.encodeIfPresent(isButtonDisabled, forKey: .button_disabled)
			try container.encodeIfPresent(isAutoHomeWifiConnect, forKey: .auto_home_wifi_connect)
			try container.encodeIfPresent(isRemoteOpening, forKey: .remote_opening)
			
		} catch {
			print("Encode error \(error)")
		}
	}
}
