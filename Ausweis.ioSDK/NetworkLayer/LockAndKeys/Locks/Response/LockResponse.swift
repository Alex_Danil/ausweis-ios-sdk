//
//  LockResponse.swift
//  Ausweis.ioSDK
//
//  Created by admin1 on 5/15/18.
//

import Foundation
import CoreLocation

public enum LockAccessType: String {
	case owner = "owner",
	guest = "guest"
}

public enum LockMode: String {
	case online = "online",
	offline = "offline"
}


open class LockOwner : Mappable {
	
	var email: String
	var ID: Int
	var phone: String?
	var name: String
	var photo: String?
	
	enum CodingKeys: String, CodingKey {
		case email
		case id
		case phone
		case name
		case photo
	}
	
	public required init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		
		email = try container.decode(String.self, forKey: .email)
		ID = try container.decode(Int.self, forKey: .id)
		photo = try container.decodeIfPresent(String.self, forKey: .photo)
		name = try container.decode(String.self, forKey: .name)
		phone = try container.decodeIfPresent(String.self, forKey: .phone)
	}
	
}

open class LockWifi : Mappable, Encodable {
	
	var homeWifiSSID: String?
	var homeWifiPassword: String?
	var lockWifiSSID: String?
	var	lockWifiPassword: String?
	
	enum CodingKeys: String, CodingKey {
		case wifi_router_ssid
		case wifi_router_password
		case ssid
		case password
	}
	
	public required init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		
		homeWifiSSID = try container.decodeIfPresent(String.self, forKey: .wifi_router_ssid)
		homeWifiPassword = try container.decodeIfPresent(String.self, forKey: .wifi_router_password)
		lockWifiSSID = try container.decodeIfPresent(String.self, forKey: .ssid)
		lockWifiPassword = try container.decodeIfPresent(String.self, forKey: .password)
	}
	
	public func encode(to encoder: Encoder) throws {
		var container = encoder.container(keyedBy: CodingKeys.self)
		do {
			try container.encode(homeWifiSSID, forKey: .wifi_router_ssid)
			try container.encode(homeWifiPassword, forKey: .wifi_router_password)
			try container.encode(lockWifiSSID, forKey: .ssid)
			try container.encode(lockWifiPassword, forKey: .password)
		} catch {
			print("Encoding error : \(error)")
		}
		
	}
	
}

open class LockResponse : Mappable {
	
	public var name: String
	var version: Int
	public var ID: Int
	var qr: String?
//	var stateChanged: Date
//	var onlineChanged: Date
//	var accessType: LockAccessType
	var isHomeAutoConnect: Bool

	var isOpened: Bool? // state
	var isActivated: Bool
	var owner: LockOwner
	
	var location: CLLocation?
	var isAllowGuestSetAlarm: Bool
	var isRemoteOpening: Bool
//	var timeZone: TimeZone
	var isActivityNotification: Bool
	var isSecurityMode: Bool?
	var mode: LockMode
	var isOnline: Bool
	var address: String?
	var wifiSettings: LockWifi?
	
	enum CodingKeys: String, CodingKey {
		case name
		case version
		case id
		case qr
//		case state_changed
//		case online_changed
		case access_type
		case auto_home_wifi_connect
		
		case state //isOpened
		case activated //isActivated
		case owner_info
		case wifi
		case lat
		case long
		case allow_guest_set_alarm
		case remote_opening
		case time_zone
		case activity_notification
		case security
		case mode
		case online
		case address
		
	}
	
	public required init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		
		ID = try container.decode(Int.self, forKey: .id)
		name = try container.decode(String.self, forKey: .name)
		version = try container.decode(Int.self, forKey: .version)
			
		qr = try container.decodeIfPresent(String.self, forKey: .qr)
//		let stateChangedString = try container.decode(String.self, forKey: .state_changed)
//		let onlineChangedString = try container.decode(String.self, forKey: .online_changed)
		
//		stateChanged = .aswDateFormatter(stateChangedString)
//		onlineChanged = .aswDateFormatter(onlineChangedString)
		
//		accessType = (try LockAccessType(rawValue: container.decode(String.self, forKey: .access_type)))!
		isHomeAutoConnect = try container.decode(Bool.self, forKey: .auto_home_wifi_connect)
		
		isOpened = try container.decodeIfPresent(Bool.self, forKey: .state)
		isActivated = try container.decode(Bool.self, forKey: .activated)
		owner = try container.decode(LockOwner.self, forKey: .owner_info)
		
		wifiSettings = try container.decodeIfPresent(LockWifi.self, forKey: .wifi)
		
		if 	let lat = try container.decodeIfPresent(Double.self, forKey: .lat),
				let lon = try container.decodeIfPresent(Double.self, forKey: .long) {
			location = CLLocation(latitude: lat, longitude: lon)
		}
		
		isAllowGuestSetAlarm = try container.decode(Bool.self, forKey: .allow_guest_set_alarm)
		isRemoteOpening = try container.decode(Bool.self, forKey: .remote_opening)
//		timezone = try container.decode(String.self, forKey: .time_zone)
		isActivityNotification = try container.decode(Bool.self, forKey: .activity_notification)
		isSecurityMode = try container.decodeIfPresent(Bool.self, forKey: .security)
		
		mode = (try LockMode(rawValue: container.decode(String.self, forKey: .mode)))!
		isOnline = try container.decode(Bool.self, forKey: .online)
		address = try container.decodeIfPresent(String.self, forKey: .address)
		
	}
	
}


open class LockDetailsResponse: Mappable {
	var lock: LockDetail
	var tariff: Tariff
	var participants : [Participant]
	
	enum CodingKeys: String, CodingKey {
		case lock,
		tariff,
		keys
	}
	
	public required init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		
		lock = try container.decode(LockDetail.self, forKey: .lock)
		tariff = try container.decode(Tariff.self, forKey: .tariff)
		participants = try container.decode([Participant].self, forKey: .keys)
	}
	
}

open class LockDetail: Mappable {
	
	public var name: String
	var isOnline: Bool
	var mode: LockMode
	//	var accessType: LockAccessType
	var location: CLLocation?
	var isActivityNotification: Bool
	var isButtonEnabled: Bool?
	var isQR: Bool?
	var version: Int
	var address: String?

	enum CodingKeys: String, CodingKey {
		case name
		case version
		case has_qr
		case access_type
		case lat
		case long
		case activity_notification
		case button_disabled
		case mode
		case online
		case address
	}

	
	public required init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		
		name = try container.decode(String.self, forKey: .name)
		version = try container.decode(Int.self, forKey: .version)
		
		isQR = try container.decodeIfPresent(Bool.self, forKey: .has_qr)
		
		//		accessType = (try LockAccessType(rawValue: container.decode(String.self, forKey: .access_type)))!
		if 	let lat = try container.decodeIfPresent(Double.self, forKey: .lat),
			let lon = try container.decodeIfPresent(Double.self, forKey: .long) {
			location = CLLocation(latitude: lat, longitude: lon)
		}
		
		isActivityNotification = try container.decode(Bool.self, forKey: .activity_notification)
		if let isButtonDisabled = try container.decodeIfPresent(Bool.self, forKey: .button_disabled) {
				isButtonEnabled = !isButtonDisabled
		}
		
		mode = (try LockMode(rawValue: container.decode(String.self, forKey: .mode)))!
		isOnline = try container.decode(Bool.self, forKey: .online)
		address = try container.decodeIfPresent(String.self, forKey: .address)
		
	}

}

open class Tariff: Mappable {
	var subscription: Subscription
	
	enum CodingKeys: String, CodingKey {
		case subscription
	}
	
	public required init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		
		subscription = try container.decode(Subscription.self, forKey: .subscription)
	}
	
}

open class Subscription : Mappable {
	var ID: Int
	var name: String
	var price: String
	var priceForYear: String
	var administratorsAmount: Int
	var remoteOpeningAllowed: Bool
	var keysAmount: Int?
	var keysRenewal: Bool
	var alarmControlAllowed: Bool
	var workingHoursTracking: Bool
	var daysOfActivity: Int
	
	
	enum CodingKeys: String, CodingKey {
		case id
		case name
		case price
		case year_price
		case n_administrators
		case remote_opening
		case n_keys
		case key_recruiting
		case security_notifications
		case working_hours
		case activity_days
		
	}
	
	public required init(from decoder: Decoder) throws {
		
		let container = try decoder.container(keyedBy: CodingKeys.self)
		
		ID = try container.decode(Int.self, forKey: .id)
		name = try container.decode(String.self, forKey: .name)
		
		price = try container.decode(String.self, forKey: .price)
		priceForYear = try container.decode(String.self, forKey: .year_price)
		administratorsAmount = try container.decode(Int.self, forKey: .n_administrators)
		remoteOpeningAllowed = try container.decode(Bool.self, forKey: .remote_opening)
		keysAmount = try container.decodeIfPresent(Int.self, forKey: .n_keys)
		keysRenewal = try container.decode(Bool.self, forKey: .key_recruiting)
		alarmControlAllowed = try container.decode(Bool.self, forKey: .security_notifications)
		workingHoursTracking = try container.decode(Bool.self, forKey: .working_hours)
		daysOfActivity = try container.decode(Int.self, forKey: .activity_days)
		
	}
}













