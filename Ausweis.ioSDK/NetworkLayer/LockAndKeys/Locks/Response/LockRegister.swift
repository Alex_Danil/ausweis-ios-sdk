//
//  LockRegister.swift
//  Ausweis.ioSDK
//
//  Created by admin1 on 5/31/18.
//

import Foundation
import CoreLocation

public enum LockSubType: String {
	case high = "high"
}

public enum LockType: String {
	case accessControl = "access_control"
}

let lockreg = LockRegister.init

public struct LockRegister: Encodable {
	public let timing: Int
	public let address: String?
	public let mac: String
	public let mode: LockMode
	public let timeZone: String
	public let subtype: LockSubType
	public let type: LockType
	public let location: CLLocation?
	public let name: String
	public let wifi: LockWifi?
	
	public init(timing: Int, address: String?, mac: String, mode: LockMode, timeZone: String, subtype: LockSubType, type: LockType, location: CLLocation?, name: String, wifi: LockWifi?) {
		self.timing = timing
		self.address = address
		self.mac = mac
		self.mode = mode
		self.timeZone = timeZone
		self.subtype = subtype
		self.type = type
		self.location = location
		self.name = name
		self.wifi = wifi

//		super.init(timing: timing, address: address, mac: mac, mode: mode, timeZone: timeZone, subtype: subtype, type: type, location: location, name: name, wifi: wifi)
	}

	public enum EncodingKeys: String, CodingKey {
		case timing
		case address
		case mac
		case mode
		case time_zone
		case subtype
		case type
		case lat
		case long
		case name
		case wifi
	}
	
	public func encode(to encoder: Encoder) throws {
		do {
			var container = encoder.container(keyedBy: EncodingKeys.self)
			try container.encodeIfPresent(timing, forKey: .timing)
			try container.encodeIfPresent(address, forKey: .address)
			try container.encode(mac, forKey: .mac)
			try container.encode(mode.rawValue, forKey: .mode)
			try container.encodeIfPresent(timeZone, forKey: .time_zone)
			try container.encodeIfPresent(subtype.rawValue, forKey: .subtype)
			try container.encodeIfPresent(type.rawValue, forKey: .type)
			
			try container.encodeIfPresent(location?.coordinate.latitude, forKey: .lat)
			try container.encodeIfPresent(location?.coordinate.longitude, forKey: .long)
			try container.encode(name, forKey: .name)
			try container.encodeIfPresent(wifi, forKey: .wifi)
		} catch {
			print("Encode error \(error)")
		}
	}
}
//"wifi":{"password":"f005e31b3c","ssid":"AUSWEIS-e31b3c","wifi_router_password":"impthepice2016","wifi_router_ssid":"TIW2018"}

