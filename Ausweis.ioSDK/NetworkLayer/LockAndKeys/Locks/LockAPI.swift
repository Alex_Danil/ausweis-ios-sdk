//
//  LockAPI.swift
//  Ausweis.ioSDK
//
//  Created by admin1 on 5/22/18.
//

import Foundation

enum LockAPI {
	case getLockDetails( accessToken: String, lockID: Int)
	case getLockHistory( accessToken: String, lockID: Int, offset: Int, limit: Int)
	case registerLock( accessToken: String, parameters: Data)
	case getLock( accessToken: String, lockID: Int)
	case updateLock( accessToken: String, parameters: Data, lockID: Int)
}

extension LockAPI : EndpointType {
	
	var headers: HTTPHeaders? {
		switch self {
		case .getLockDetails(let accessToken, _), .getLockHistory(let accessToken, _ ,_ ,_) , .registerLock(let accessToken, _), .getLock(let accessToken, _), .updateLock(let accessToken, _, _):
			return ["Authorization": "JWT \(accessToken)"]
		}
	}
	
	var baseURL: URL {
		return URL(string: URLConfiguration.Environment.Develop.rawValue)!
	}
	
	var path: String {
		switch self {
		case .getLockDetails(_ , let lockID):
			return "/api/v1/locks/owner/\(lockID)/full_details/"
		case .getLockHistory:
			return "/api/v1/history/"
		case .registerLock:
			return "/api/v1/locks/owner/"
		case .getLock(_ , let lockID), .updateLock(_,_, let lockID):
			return "/api/v1/locks/owner/\(lockID)/"
		}
	}
	
	var httpMethod: HTTPMethod {
		switch self {
		case .getLockDetails, .getLockHistory, .getLock:
			return .get
		case .registerLock:
			return .post
		case .updateLock:
			return .patch
		}
	}
	
	var task: HTTPTask {
		switch self {
		
		case .getLockDetails, .getLock:
			return .requestParametersAndHeaders(bodyParameters: nil, urlParameters: nil, additionalHeaders: headers)
		case .getLockHistory(_, let lockID, let offset, let limit):
			let params = ["limit":"\(limit)","offset":"\(offset)","lock":"\(lockID)"]
			return .requestParametersAndHeaders(bodyParameters: nil, urlParameters: params, additionalHeaders: headers)
		case .registerLock(_, let body), .updateLock(_, let body, _):
			return .requestBody(bodyParameters: body, additionalHeaders: headers)
		}
	}
	
}


