//
//  LockAPIManager.swift
//  Ausweis.ioSDK
//
//  Created by admin1 on 5/22/18.
//

import Foundation

struct LockAPIManager: NetworkManager {
	
	
	
	internal var urlConfig: URLConfiguration {
		return URLConfiguration()
	}
	
	private let router = Router<LockAPI>()
	
	init () {
	}
	

	
	public func getLockDetails(_ lockID: Int, completion: @escaping (_ lock: LockDetailsResponse?, _ errorResult: Result<String>?, _ errorBody: ResponseError? ) -> ()) {
		self.router.request( .getLockDetails(accessToken: AccessManager().accessToken(), lockID: lockID) ) { (data, response, error) in
			if error != nil {
				completion(nil, Result.failure(NetworkError.noConnection.rawValue, statusCode: -999), nil)
				return
			}
			guard let httpResponse = response as? HTTPURLResponse else {
				completion(nil, Result.failure(NetworkError.noConnection.rawValue, statusCode: -995), nil)
				return
			}
			
			self.handleResponse(responseType: LockDetailsResponse.self, data, httpResponse, completion)
			
		}
	}
	
	
	public func getLock(_ lockID: Int, completion: @escaping (_ lock: Lock?, _ errorResult: Result<String>?, _ errorBody: ResponseError? ) -> ()) {
		self.router.request( .getLock(accessToken: AccessManager().accessToken(), lockID: lockID) ) { (data, response, error) in
			if error != nil {
				completion(nil, Result.failure(NetworkError.noConnection.rawValue, statusCode: -999), nil)
				return
			}
			guard let httpResponse = response as? HTTPURLResponse else {
				completion(nil, Result.failure(NetworkError.noConnection.rawValue, statusCode: -995), nil)
				return
			}
			
			self.handleResponse(responseType: Lock.self, data, httpResponse, completion)
			
		}
	}
	
	
	public func getLockHistory(_ lockID: Int, offset: Int, limit: Int, completion: @escaping (_ lockHistory: LockHistoryResponse?, _ errorResult: Result<String>?, _ errorBody: ResponseError? ) -> ()) {
		self.router.request(.getLockHistory(accessToken: AccessManager().accessToken(), lockID: lockID, offset: offset, limit: limit)) { (data, response, error) in
			if error != nil {
				completion(nil, Result.failure(NetworkError.noConnection.rawValue, statusCode: -999), nil)
				return
			}
			guard let httpResponse = response as? HTTPURLResponse else {
				completion(nil, Result.failure(NetworkError.noConnection.rawValue, statusCode: -995), nil)
				return
			}
			
			
			self.handleResponse(responseType: LockHistoryResponse.self, data, httpResponse, completion)
		}
	}
	
	public func registerLock(_ parameters: LockRegister, completion: @escaping (_ lockActivation: LockActivation?, _ errorResult: Result<String>?, _ errorBody: ResponseError? ) -> ()) {
		do {
			let body = try JSONEncoder().encode(parameters)
			self.router.request(.registerLock(accessToken: AccessManager().accessToken(), parameters: body), completion: { (data, response, error) in
				if error != nil {
					completion(nil, Result.failure(NetworkError.noConnection.rawValue, statusCode: -999), nil)
					return
				}
				guard let httpResponse = response as? HTTPURLResponse else {
					completion(nil, Result.failure(NetworkError.noConnection.rawValue, statusCode: -995), nil)
					return
				}
				
				self.handleResponse(responseType: LockActivation.self, data, httpResponse, completion)
//
//				switch httpResponse.statusCode {
//				case 200...201:
//					completion(true,nil,nil)
//				default:
//					self.handleError(responseData: data, httpResponse, { (result, error) in
//						completion(false, result, error)
//					})
//				}
			})

		} catch {
			completion(nil, Result.failure(NetworkResponse.unableToDecode.rawValue, statusCode: -997), nil)
		}		
	}
	
	public func updateLock(_ parameters: Lock, completion: @escaping (_ lockActivation: Lock?, _ errorResult: Result<String>?, _ errorBody: ResponseError? ) -> ()) {
		do {
			let body = try JSONEncoder().encode(parameters)
			self.router.request(.updateLock(accessToken: AccessManager().accessToken(), parameters: body, lockID: parameters.ID), completion: { (data, response, error) in
				if error != nil {
					completion(nil, Result.failure(NetworkError.noConnection.rawValue, statusCode: -999), nil)
					return
				}
				guard let httpResponse = response as? HTTPURLResponse else {
					completion(nil, Result.failure(NetworkError.noConnection.rawValue, statusCode: -995), nil)
					return
				}
				
				self.handleResponse(responseType: Lock.self, data, httpResponse, completion)
				//
				//				switch httpResponse.statusCode {
				//				case 200...201:
				//					completion(true,nil,nil)
				//				default:
				//					self.handleError(responseData: data, httpResponse, { (result, error) in
				//						completion(false, result, error)
				//					})
				//				}
			})
			
		} catch {
			completion(nil, Result.failure(NetworkResponse.unableToDecode.rawValue, statusCode: -997), nil)
		}
	}
	
}

extension LockAPIManager {
	
	func handleNetworkResponse(_ response: HTTPURLResponse) -> Result<String> {
		
		switch  response.statusCode {
		case 400			: return .failureResponse
		case 200...299: return .success
		case 401			: return .failure(NetworkResponse.authenticationError.rawValue, statusCode: response.statusCode)
		case 403			: return .failureResponse
		case 500...599: return .failure(NetworkResponse.badRequest.rawValue, statusCode: response.statusCode)
		default:
			return .failure(NetworkResponse.failed.rawValue, statusCode: response.statusCode)
		}
	}
	
	
	func handleResponse<T>(responseType: T.Type ,_ responseData: Data?, _ response: HTTPURLResponse, _ completion: @escaping (T?, Result<String>?, ResponseError?) -> ()) where T : Decodable {
		
		let result = self.handleNetworkResponse(response)
		switch result {
		case .success :
			guard let responseData = responseData else {
				completion(nil, Result.failure(NetworkResponse.noData.rawValue, statusCode: -998), nil)
				return
			}
			
			do {
				let apiResponse = try JSONDecoder().decode(T.self, from: responseData)
				completion(apiResponse, nil, nil)
			} catch {
				completion(nil, Result.failure(NetworkResponse.unableToDecode.rawValue, statusCode: -997), nil)
			}
		case .failureResponse:
			handleError(responseData: responseData, response, { (result, error) in
				completion(nil, result,error)
			})
		case .failure(_, _) :
			completion(nil, result, nil)
		}
	}
	
}
