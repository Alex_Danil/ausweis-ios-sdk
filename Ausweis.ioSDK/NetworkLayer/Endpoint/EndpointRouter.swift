//
//  EndpointRouter.swift
//  Ausweis.ioSDK
//
//  Created by admin1 on 4/13/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import Foundation
import MobileCoreServices


class Router<EndPoint: EndpointType>: NetworkRouter {
	
	private var task: URLSessionTask?
	
	func request(_ route: EndPoint, completion: @escaping NetworkRouterCompletion) {
		let session = URLSession.shared
		
		let cookieStore = HTTPCookieStorage.shared
		for cookie in cookieStore.cookies ?? [] {
			cookieStore.deleteCookie(cookie)
		}
		
		do {
			let request = try self.buildRequest(from:route)
			
			task = session.dataTask(with: request, completionHandler: { data, response, error in
				completion(data,response,error)
			})
		} catch {
			completion(nil,nil,error)
		}
		
		task?.resume()
	}
	
	fileprivate func buildRequest(from route: EndPoint) throws -> URLRequest {
		var request = URLRequest.init(url: route.baseURL.appendingPathComponent(route.path),
																	cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 15.0)
		request.httpMethod = route.httpMethod.rawValue
		
		do {
			switch route.task {
			case .request:
				request.setValue("application/json", forHTTPHeaderField: "Content-Type")
			case .requestParameters(let bodyParameters, let urlParameters):
				try self.configureParameters(bodyParameters: bodyParameters,
																		 urlParameters : urlParameters,
																		 request: &request)
			case .requestParametersAndHeaders(let bodyParameters, let urlParameters, let additionalHeaders):
				self.addAdditionalHeaders(additionalHeaders, request: &request)
				try self.configureParameters(bodyParameters: bodyParameters,
																				urlParameters : urlParameters,
																				request: &request)
			case .requestBody(let body, let additionalHeaders):
				JSONParameterEncoder.encode(urlRequest: &request, with: body ?? Data())
				if let headers = additionalHeaders {
					self.addAdditionalHeaders(headers, request: &request)
				}
			case .multipartFromData(let filepath, _ ,_):
				try self.createMultipartRequest(withFilePath: filepath, request: &request)
			case .multipartFromBody(let parameters, _ , let additionalHeaders):
				self.addAdditionalHeaders(additionalHeaders, request: &request)
				try self.createMultipartRequest(withBody: parameters, request: &request)
			}
		
			return request
		} catch {
			throw error
		}
	}
	
	fileprivate func configureParameters(bodyParameters: Parameters?,
																			 urlParameters:Parameters?,
																			 request: inout URLRequest) throws {
		
		do {
			if let urlParameters = urlParameters {
				try URLParameterEncoder.encode(urlRequest: &request, with: urlParameters)
			}
			if let bodyParameters = bodyParameters {
					try JSONParameterEncoder.encode(urlRequest: &request, with: bodyParameters)
			}
			
		} catch  {
			throw error
		}
	}
	
	fileprivate func addAdditionalHeaders(_ additionalHeaders: HTTPHeaders?, request: inout URLRequest) {
		guard let headers = additionalHeaders else { return }
		headers.forEach { (key,value) in
			request.setValue(value, forHTTPHeaderField: key)
		}
	}
	
	
	func cancel() {
		self.task?.cancel()
	}
	
}


extension Router {
	
	private func createMultipartRequest(withFilePath filepath: String, request: inout URLRequest) throws -> () {

		let boundary = generateBoundaryString()

		request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
		
		request.httpBody = try createBody(forPath: [filepath], boundary: boundary)
		
	}
	
	private func createMultipartRequest(withBody params: Parameters?, request: inout URLRequest) throws -> () {
		
		let boundary = generateBoundaryString()
		
		request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
	
		request.httpBody = try createBody(forParams: params, boundary: boundary)
		
	}

	
	private func createBody(forPath paths: [String], boundary: String) throws -> Data {
		let body = NSMutableData()

		for path in paths {
			let url = URL(fileURLWithPath: path)
			let filename = url.lastPathComponent
			let data = try Data(contentsOf: url)
			let mimetype = mimeType(for: path)
			
			body.append("--\(boundary)\r\n".data(using: .utf8)!)
			body.append("Content-Disposition: form-data; name=\"image\"; filename=\"\(filename)\"\r\n".data(using: .utf8)!)
			body.append("Content-Type: \(mimetype)\r\n\r\n".data(using: .utf8)!)
			body.append(data)
			body.append("\r\n".data(using: .utf8)!)
		}

		return body as Data
	}
	
	private func createBody(forParams params: Parameters?, boundary: String) throws -> Data {
		let body = NSMutableData()
		
		guard let parameters = params else {
			throw NetworkError.parametersNil
		}
		
		let boundaryPrefix = "--\(boundary)\r\n"
		
		for (key, value) in parameters {
			body.appendString(boundaryPrefix)
			body.appendString("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
			body.appendString("\(value)\r\n")
		}
		body.appendString(boundaryPrefix)

		return body as Data
	}
	
	/// Create boundary string for multipart/form-data request
	///
	/// - returns:            The boundary string that consists of "Boundary-" followed by a UUID string.
	
	private func generateBoundaryString() -> String {
		return "Boundary-\(UUID().uuidString)"
	}
	
	/// Determine mime type on the basis of extension of a file.
	///
	/// This requires `import MobileCoreServices`.
	///
	/// - parameter path:         The path of the file for which we are going to determine the mime type.
	///
	/// - returns:                Returns the mime type if successful. Returns `application/octet-stream` if unable to determine mime type.
	
	private func mimeType(for path: String) -> String {
		let url = URL(fileURLWithPath: path)
		let pathExtension = url.pathExtension
		
		if let uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, pathExtension as NSString, nil)?.takeRetainedValue() {
			if let mimetype = UTTypeCopyPreferredTagWithClass(uti, kUTTagClassMIMEType)?.takeRetainedValue() {
				return mimetype as String
			}
		}
		return "application/octet-stream"
	}
	
}


extension NSMutableData {
	func appendString(_ string: String) {
		let data = string.data(using: String.Encoding.utf8, allowLossyConversion: false)
		append(data!)
	}
}
