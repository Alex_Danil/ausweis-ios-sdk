//
//  NetworkManager.swift
//  Ausweis.ioSDK
//
//  Created by admin1 on 4/13/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import Foundation

protocol NetworkManager {
	var urlConfig : URLConfiguration { get }
	
	func handleResponse<T:Mappable>(responseType: T.Type ,_ responseData: Data?, _ response: HTTPURLResponse, _ completion: @escaping (_ responseObject: T?, _ errorResult: Result<String>?, _ errorBody: ResponseError? ) -> ())
	
	func handleNetworkResponse(_ response: HTTPURLResponse) -> Result<String>
	
	func handleError(responseData: Data?, _ response: HTTPURLResponse, _ completion: @escaping (_ errorResult: Result<String>?, _ errorBody: ResponseError? ) -> ())
//	static var router = Router<T:EndpointType>
}


extension NetworkManager {
	
	func handleNetworkResponse(_ response: HTTPURLResponse) -> Result<String> {
		switch  response.statusCode {
		case 200...299: return .success
		case 403			: return .failure(NetworkResponse.authenticationError.rawValue, statusCode: response.statusCode)
		case 500...599: return .failure(NetworkResponse.badRequest.rawValue, statusCode: response.statusCode)
		default:				return .failure(NetworkResponse.failed.rawValue, statusCode: response.statusCode)
		}		
	}
		
	func handleError(responseData: Data?, _ response: HTTPURLResponse, _ completion: @escaping (_ errorResult: Result<String>?, _ errorBody: ResponseError? ) -> ()) {
		do {
			let apiResponseError = try JSONDecoder().decode(ResponseError.self, from: responseData!)
			completion(nil, apiResponseError);
		} catch {
			completion(Result.failure(NetworkResponse.unableToDecodeError.rawValue, statusCode: -996), nil)
		}
	}
	
	func handleResponse<T>(responseType: T.Type ,_ responseData: Data?, _ response: HTTPURLResponse, _ completion: @escaping (T?, Result<String>?, ResponseError?) -> ()) where T : Decodable {
		
		let result = self.handleNetworkResponse(response)
		switch result {
		case .success :
			guard let responseData = responseData else {
				completion(nil, Result.failure(NetworkResponse.noData.rawValue, statusCode: -998), nil)
				return
			}
			
			do {
				let apiResponse = try JSONDecoder().decode(T.self, from: responseData)
				completion(apiResponse, nil, nil)
			} catch {
				completion(nil, Result.failure(NetworkResponse.unableToDecode.rawValue, statusCode: -997), nil)
			}
		case .failureResponse:
			handleError(responseData: responseData, response, { (result, error) in
				completion(nil, result,error)
			})
		case .failure(_, _) :
			completion(nil, result, nil)
		}
	}
	
}
