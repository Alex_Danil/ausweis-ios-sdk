//
//  EndpointBase.swift
//  Ausweis.ioSDK
//
//  Created by admin1 on 4/13/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import Foundation

public typealias HTTPHeaders = [String:String]

protocol EndpointType {
	var baseURL: URL {get}
	var path: String {get}
	var httpMethod: HTTPMethod {get}
	var task: HTTPTask {get}
	var headers: HTTPHeaders? {get}
}

public enum HTTPMethod : String {
	case get = "GET"
	case post = "POST"
	case put = "PUT"
	case patch = "PATCH"
	case delete = "DELETE"
}

public enum HTTPTask {
	case request
	case requestBody(bodyParameters: Data?, additionalHeaders: HTTPHeaders?)
	case requestParameters(bodyParameters: Parameters?, urlParameters: Parameters?)
	case requestParametersAndHeaders(bodyParameters: Parameters?, urlParameters: Parameters?, additionalHeaders:HTTPHeaders?)
	
	case multipartFromData(filePath: String, bodyParameters: Parameters?, urlParameters: Parameters?)
	case multipartFromBody(bodyParameters: Parameters?, urlParameters: Parameters?, additionalHeaders:HTTPHeaders?)

}
