//
//  Result.swift
//  Ausweis.ioSDK
//
//  Created by admin1 on 4/16/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import Foundation

enum Result<String> {
	case success
	case failureResponse
	case failure(String, statusCode : Int)
	
	func failureValue() -> (message:String, code: Int)? {
		switch self {
		case .failure(let message, let statusCode):
			return (message,statusCode)
		default:
			return nil
		}
	}
}
