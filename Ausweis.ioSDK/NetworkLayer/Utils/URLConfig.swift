//
//  URLConfig.swift
//  Ausweis.ioSDK
//
//  Created by admin1 on 4/13/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import Foundation


struct URLConfiguration {

//	#if DEBUG
	let environment = Environment.Develop.rawValue
	
	enum Environment: String {
		case Production = "https://api.ausweis.io"
		case Develop 		= "https://dev-test.ausweis.io"
	}
	
//we can change this at any time
//	#else
//
//	let environment = NSBundle.mainBundle().infoDictionary!  ["MY_API_BASE_URL_ENDPOINT"] as! String
//	#endif
}
