//
//  ParametersEncoding.swift
//  Ausweis.ioSDK
//
//  Created by admin1 on 4/13/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import Foundation

public typealias Parameters = [String : Any]

public protocol ParameterEncoder {
	static func encode(urlRequest: inout URLRequest, with parameters: Parameters) throws
}
