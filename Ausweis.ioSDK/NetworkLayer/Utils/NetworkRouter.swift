//
//  Router.swift
//  Ausweis.ioSDK
//
//  Created by admin1 on 4/13/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import Foundation

public typealias NetworkRouterCompletion = (_ data: Data?,_ response: URLResponse?,_ error: Error?)->()

protocol NetworkRouter : class {
	associatedtype EndPoint: EndpointType
	func request(_ route: EndPoint, completion: @escaping NetworkRouterCompletion)
	func cancel()	
}
