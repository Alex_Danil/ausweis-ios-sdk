//
//  Mappable.swift
//  Ausweis.ioSDK
//
//  Created by admin1 on 4/13/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import Foundation


public protocol Mappable: Decodable {
	init?(jsonString: String)
	
	
}

extension Mappable {
	public init?(jsonString: String) {
		guard let data = jsonString.data(using: .utf8) else {
			return nil
		}
		
		do {
			self = try JSONDecoder().decode(Self.self, from: data)
		} catch {
			print(error)
			return nil
		}
		
		// I used force unwrap for simplicity.
		// It is better to deal with exception using do-catch.
	}
	

}
