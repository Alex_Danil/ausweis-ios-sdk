//
//  Errors.swift
//  Ausweis.ioSDK
//
//  Created by admin1 on 4/13/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import Foundation

public enum NetworkError : String, Error {
	case noConnection = "Please check your internet connection."
	case parametersNil = "Parameters were nil."
	case encodingFailed = "Parameters encoding failed."
	case missingURL = "URL is nil."
}

public enum NetworkResponse : String, Error {
	case success
	case authenticationError = "You're not authenticated"
	case badRequest = "Bad request"
	case outdated = "The url you requested is outdated"
	case failed = "Request failed"
	case noData = "No data to decode"
	case unableToDecode = "Could not decode response"
	case unableToDecodeError = "Could not decode Error response"
}
