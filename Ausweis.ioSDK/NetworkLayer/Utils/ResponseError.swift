//
//  ASWResponseError.swift
//  Ausweis.ioSDK
//
//  Created by admin1 on 4/16/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import Foundation

public struct ResponseError: Mappable {
	let error: String
	let type: String
	let statusCode: Int
	
	public let errors: [DetailedError]
	
	enum CodingKeys: String, CodingKey {
		case error = "error_code"
		case statusCode = "status_code"
		case type
		case errors
	}
	
	public init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		
		error = try values.decode(String.self, forKey: .error)
		type = try values.decode(String.self, forKey: .type)
		statusCode = try values.decode(Int.self, forKey: .statusCode)
		errors = try values.decode([DetailedError].self, forKey: .errors)
	}
	
	init(statusCode: Int, type: String? = nil , error: String) {
		self.statusCode = statusCode
		self.type = type ?? "general"
		self.error = error
		
		let detailedError = DetailedError(error: self.type, message: self.error)
		self.errors = [detailedError]
	}
	
}

public struct DetailedError : Mappable {
	public let message: String
	let error: String
	
	enum CodingKeys: String, CodingKey {
		case error = "error_code"
		case message
	}
	
	init(error: String, message: String) {
		self.error = error
		self.message = message
	}
	
	public init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		
		error = try values.decode(String.self, forKey: .error)
		message = try values.decode(String.self, forKey: .message)
		
	}
}
