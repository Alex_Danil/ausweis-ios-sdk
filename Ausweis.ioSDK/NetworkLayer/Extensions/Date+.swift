//
//  Date+.swift
//  Ausweis.ioSDK
//
//  Created by admin1 on 5/16/18.
//

import Foundation


extension Date {
	
	public static func aswDateFormatter(_ dateString: String) -> Date! {
		let formatter = DateFormatter()
		formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
		
		return formatter.date(from: dateString)
	}
	
	public static func externalDateFormatter() -> DateFormatter {
		let formatter = DateFormatter()
		formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
		
		return formatter
	}
	
}
