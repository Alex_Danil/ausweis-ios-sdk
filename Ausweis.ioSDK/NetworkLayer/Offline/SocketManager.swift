//
//  HotspotManager.swift
//  Ausweis.ioSDK
//
//  Created by admin1 on 5/23/18.
//

import Foundation

public class SocketManager {
	
	internal var client = TCPClient(address: "192.168.1.1", port: 5005)
	
	private var connectionTried = 4;
	private var isConnected = false;
	
	static let shared = SocketManager()
	private let streamManager = StreamManager()
	
	private let readingQueue = DispatchQueue.global(qos: .background)
	private var onConnect : (()->())?
	
	private init() {

	}
	
	@discardableResult func connect() -> SocketManager {
		print("Start \(Date())")
		DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
			self.streamManager.openStream(self.client)
		})
		
		return self
	}
	
	func sendMessage(_ data: Data) -> Future<String> {

		readingQueue.asyncAfter(deadline: .now() + 2 ) {
			print("Data sending \(Date())")
			if self.streamManager.isConnected {
				self.streamManager.sendData(data: data)
			} else {
				self.streamManager.onConnect = {
					self.streamManager.sendData(data: data)
				}
			}
		}
		
		return Future(operation: { (completion) in
//			print("Future started ()")
			self.streamManager.messageRecieved = { message in
				completion(.success(message))
			}
		})
	}
	
	@discardableResult func disconnect() -> SocketManager {
		//		socket.disconnect(forceTimeout: 0, closeCode: 0)
		return self
	}
	
}
