//
//  HotspotManager.swift
//  Ausweis.ioSDK
//
//  Created by admin1 on 5/23/18.
//

import Foundation

#if (arch(i386) || arch(x86_64)) && os(iOS)
#else
	import SystemConfiguration.CaptiveNetwork
	import NetworkExtension
#endif


class HotspotManager {
	
	
	
	@discardableResult func connect(_ SSID: String, password: String?, completion: @escaping ((_ success:Bool, _ error:Error?) -> ()) ) -> HotspotManager {
		
		if fetchSSIDInfo() == SSID {
			completion(true,nil)
			return self
		}
		#if (arch(i386) || arch(x86_64)) && os(iOS)
		#else
			let configuration = NEHotspotConfiguration(ssid: SSID, passphrase: password ?? "", isWEP: false)
			configuration.joinOnce = true
			
			NEHotspotConfigurationManager.shared.apply(configuration) { (error) in
				guard let error = error else {
					completion(true, nil); return
				}
				
				completion(false, error)
			}
		#endif
		

		return self
	}
	
	private func fetchSSIDInfo() ->  String? {
		#if (arch(i386) || arch(x86_64)) && os(iOS)
		#else
		if let interfaces = CNCopySupportedInterfaces() {
			for i in 0..<CFArrayGetCount(interfaces){
				let interfaceName: UnsafeRawPointer = CFArrayGetValueAtIndex(interfaces, i)
				let rec = unsafeBitCast(interfaceName, to: AnyObject.self)
				let unsafeInterfaceData = CNCopyCurrentNetworkInfo("\(rec)" as CFString)
				
				if let unsafeInterfaceData = unsafeInterfaceData as? Dictionary<AnyHashable, Any> {
					return unsafeInterfaceData["SSID"] as? String
				}
			}
		}
		#endif
		return nil
	}
	
}



