//
//  StreamManager.swift
//  Ausweis.ioSDK
//
//  Created by admin1 on 5/25/18.
//

import Foundation

struct TCPClient {
	let address: String
	let port: UInt32
}

protocol StreamDataDelegate  {
	func messageDidRecieved(_ message: String?)
	
}

class StreamManager : NSObject {
	
	var delegate : StreamDataDelegate?
	
	var readStream : Unmanaged<CFReadStream>?
	var writeStream : Unmanaged<CFWriteStream>?
	
	var inputStream : InputStream
	var outputStream : OutputStream
	
	var message: Data?
	
	var onConnect : (()->())?
	
	var isConnected: Bool = false
	
	var messageRecieved : ((String)->())?
	
	override init() {
		inputStream = InputStream()
		outputStream = OutputStream()
	}
	
	func openStream(_ client: TCPClient, ssl: Bool = true) {
		CFStreamCreatePairWithSocketToHost(kCFAllocatorDefault, client.address as CFString, client.port, &readStream, &writeStream)
		
		inputStream = readStream!.takeRetainedValue()
		outputStream = writeStream!.takeRetainedValue()
		
		if ssl {
			let dict = [
				kCFStreamSSLValidatesCertificateChain: kCFBooleanFalse,     // allow self-signed certificate
				kCFStreamSSLLevel: "kCFStreamSocketSecurityLevelTLSv1_2"    // don't understand, why there isn't a constant for version 1.2
				] as CFDictionary
			
			let sslSetRead = CFReadStreamSetProperty(inputStream, CFStreamPropertyKey(kCFStreamPropertySSLSettings), dict)
			let sslSetWrite = CFWriteStreamSetProperty(outputStream, CFStreamPropertyKey(kCFStreamPropertySSLSettings), dict)
			
			if sslSetRead == false || sslSetWrite == false {
				//				throw ConnectionError.sslConfigurationFailed
			}
			
		}
		
		inputStream.delegate = self
		outputStream.delegate = self
		
		// schedule the streams for the runLoop
		inputStream.schedule(in: .current, forMode: .commonModes)
		outputStream.schedule(in: .current, forMode: .commonModes)
		
		// open the streams
		inputStream.open()
		outputStream.open()
		
	}
	
	func sendMessage(message: String) {
		let data = message.data(using: .utf8)!
		
		_ = data.withUnsafeBytes { outputStream.write($0, maxLength: data.count) }
	}
	
	func sendData(data: Data) {
		_ = data.withUnsafeBytes { outputStream.write($0, maxLength: data.count) }
	}
	
	
}

extension StreamManager : StreamDelegate {
	
	func stream(_ aStream: Stream, handle eventCode: Stream.Event) {
		print("stream event \(eventCode)")
		
		switch eventCode {
		case .openCompleted:
			print("Opened")
			isConnected = true
			onConnect?()
		case .hasBytesAvailable:
			switch aStream {
			case inputStream :
				var buffer = [UInt8](repeating: 0, count: 1024)
				
				while inputStream.hasBytesAvailable {
					let bytesRead = self.inputStream.read(&buffer, maxLength: buffer.count)
					
					if let responseString = String(data:Data(buffer), encoding:.utf8) {
						messageRecieved?(responseString)
						print ("Response : \(responseString)\n BytesCount: \(bytesRead)")
					} else {
						print("Error: unreadable result")
					}
				}
				
			default:
				break
			}
		case .hasSpaceAvailable:
			print("Stream has available space")
			isConnected = true
		case .errorOccurred:
			print("Error : \(aStream.streamError?.localizedDescription)")
		case .endEncountered:
			aStream.close()
			aStream.remove(from: .current, forMode: .commonModes)
			isConnected = false
			print("Stream closed")
		default:
			isConnected = false
			print("Unknown error")
		}
	}
}

