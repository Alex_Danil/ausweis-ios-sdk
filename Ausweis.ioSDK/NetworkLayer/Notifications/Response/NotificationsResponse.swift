//
//  NotificationsResponse.swift
//  Ausweis.ioSDK
//
//  Created by admin1 on 6/4/18.
//

import Foundation

public struct NotificationsResponse : Mappable {
	var response: NotificationsList
	
	enum CodingKeys: String, CodingKey {
		case notifications
	}
	
	public init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		
		response = try container.decode(NotificationsList.self, forKey: .notifications)
	}

}

public struct NotificationsList : Mappable {
	var result: [Notification]
	
	enum CodingKeys: String, CodingKey {
		case rows
	}
	
	public init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		
		result = try container.decode([Notification].self, forKey: .rows)
	}

}

open class Notification : Mappable {
	var userPhoto: String!
	var created: String!
	var isRead: Bool!
	var lock: Int!
	var modified: String!
	var toUser: Int?
	var key: String!
	var message: String!
	var messageType: String!
	var ID: Int!
	var subject: String!
	
	
	enum CodingKeys: String, CodingKey {
		case user_photo
		case created
		case read
		case lock
		case modified
		case to_user
		case key
		case message
		case message_type
		case id
		case subject
	}
	
	required public init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		
		userPhoto = try container.decodeIfPresent(String.self, forKey: .user_photo)
		created = try container.decodeIfPresent(String.self, forKey: .created)
		isRead = try container.decodeIfPresent(Bool.self, forKey: .read)
		lock = try container.decodeIfPresent(Int.self, forKey: .lock)
		modified = try container.decodeIfPresent(String.self, forKey: .modified)
		toUser = try container.decodeIfPresent(Int.self, forKey: .to_user)
		key = try container.decodeIfPresent(String.self, forKey: .key)
		message = try container.decodeIfPresent(String.self, forKey: .message)
		messageType = try container.decodeIfPresent(String.self, forKey: .message_type)
		ID = try container.decodeIfPresent(Int.self, forKey: .id)
		subject = try container.decodeIfPresent(String.self, forKey: .subject)
	}
	
}


