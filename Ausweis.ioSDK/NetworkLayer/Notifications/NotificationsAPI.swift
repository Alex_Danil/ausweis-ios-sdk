//
//  NotificationsAPI.swift
//  Ausweis.ioSDK
//
//  Created by admin1 on 6/4/18.
//

import Foundation

enum NotificationsAPI {
	case getNotifications( accessToken: String, limit: Int, offset: Int)
}

extension NotificationsAPI : EndpointType {
	
	var headers: HTTPHeaders? {
		switch self {
		case .getNotifications(let accessToken, _, _):
			return ["Authorization": "JWT \(accessToken)"]
		}
	}
	
	var baseURL: URL {
		return URL(string: URLConfiguration.Environment.Develop.rawValue)!
	}
	
	var path: String {
		switch self {
		case .getNotifications:
			return "/api/v1/notifications/users/"
		}
	}
	
	var httpMethod: HTTPMethod {
		switch self {
		case .getNotifications:
			return .get
		}
	}
	
	var task: HTTPTask {
		switch self {
		case .getNotifications(_, let limit, let offset):
			let params = ["limit": limit, "offset": offset]
			return .requestParametersAndHeaders(bodyParameters: nil, urlParameters: params, additionalHeaders: headers)

		}
	}
	
}
