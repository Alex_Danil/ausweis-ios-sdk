//
//  NotificationsAPIManager.swift
//  Ausweis.ioSDK
//
//  Created by admin1 on 6/4/18.
//

import Foundation

struct NotificationsAPIManager: NetworkManager {
	
	internal var urlConfig: URLConfiguration {
		return URLConfiguration()
	}
	
	private let router = Router<NotificationsAPI>()
	
	init () {
		
	}
	
	public func getNotifications(_ offset: Int, limit: Int, completion: @escaping (_ lockHistory: NotificationsResponse?, _ errorResult: Result<String>?, _ errorBody: ResponseError? ) -> ()) {
		router.request(.getNotifications(accessToken: AccessManager().accessToken(), limit: limit, offset: offset)) { (data, response, error) in
			if error != nil {
				completion(nil, Result.failure(NetworkError.noConnection.rawValue, statusCode: -999), nil)
				return
			}
			guard let httpResponse = response as? HTTPURLResponse else {
				completion(nil, Result.failure(NetworkError.noConnection.rawValue, statusCode: -995), nil)
				return
			}
			
			self.handleResponse(responseType: NotificationsResponse.self, data, httpResponse, completion)
		}	
	
	}
	
}

