//
//  AccessManager.swift
//  Ausweis.ioSDK
//
//  Created by admin1 on 4/26/18.
//

import Foundation


struct AccessManager {
	
	
	private let accessTokenKey = "kAccessToken"
	
	func accessToken() -> String! {
		guard let encodedData = UserDefaults.standard.string(forKey: accessTokenKey), let tokenData = Data(base64Encoded: encodedData), let accessToken = String(data: tokenData, encoding: .utf8) else {
			return nil
		}
		
		return accessToken
	}
	
	func setAccessToken(_ accessToken: String?) {
		if let accessToken = accessToken {
			let tokenData = accessToken.data(using: .utf8)
			let codedData = tokenData?.base64EncodedString()
			UserDefaults.standard.set(codedData, forKey: accessTokenKey)
		}
	}
}
