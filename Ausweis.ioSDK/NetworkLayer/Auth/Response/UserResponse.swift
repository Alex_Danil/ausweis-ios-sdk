//
//  UserResponse.swift
//  Ausweis.ioSDK
//
//  Created by admin1 on 4/13/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import Foundation

public struct UserResponse : Mappable {
	var token: String
	var user: User?
	
	enum CodingKeys: String, CodingKey {
		case token
		case user
	}
	
	public init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		
		user = try values.decodeIfPresent(User.self, forKey: .user)
		token = try values.decode(String.self, forKey: .token)
	}
	
}

public struct User : Mappable {
	
	var name: String
	var photo: String?
	var notificationSettings: [String:Bool]?
	var email: String
	var phone: String?
	var ID: Int?
	
	enum CodingKeys: String, CodingKey {
		case userKey = "user"
		case ID = "id"
		case name
		case photo
		case email
		case phone
		case notificationSettings
	}
	
	public init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		
		name = try values.decode(String.self, forKey: .name)
		photo = try values.decodeIfPresent(String.self, forKey: .photo)
		notificationSettings = try values.decodeIfPresent([String:Bool].self, forKey: .notificationSettings)
		email = try values.decode(String.self, forKey: .email)
		phone = try values.decodeIfPresent(String.self, forKey: .phone)
		
		ID = try values.decode(Int.self, forKey: .ID)
	}
	
}
