//
//  AuthAPIManager.swift
//  Ausweis.ioSDK
//
//  Created by admin1 on 4/16/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import Foundation


struct AuthAPIManager: NetworkManager {

	
	
	internal var urlConfig: URLConfiguration {
		return URLConfiguration()
	}
	
	private let router = Router<AuthAPI>()
	
	init () {
	}
	

	public func login(_ email: String, password: String, completion: @escaping (_ user: UserResponse?, _ errorResult: Result<String>?, _ errorBody: ResponseError? ) -> ()) {
		
		self.router.request(.login(email: email, password: password), completion: { (data, response, error) in
			if error != nil {
				completion(nil, Result.failure(NetworkError.noConnection.rawValue, statusCode: -999), nil)
			}

			guard let httpResponse = response as? HTTPURLResponse else {
				completion(nil, Result.failure(NetworkError.noConnection.rawValue, statusCode: -995), nil)
				return
			}
			
			self.handleResponse(responseType: UserResponse.self, data, httpResponse, completion)
		})
	}
	
	public func register(_ email: String, password: String, phoneNumber: String?, userName: String, completion: @escaping (_ success: Bool , _ errorResult: Result<String>?,_ errorBody: ResponseError? ) -> ()) {
		self.router.request(.register(email: email, password: password, name: userName, phone: phoneNumber ?? "") , completion: { (data, response, error) in
			if error != nil {
				completion(false, Result.failure(NetworkError.noConnection.rawValue, statusCode: -999), nil)
				return
			}
			guard let httpResponse = response as? HTTPURLResponse else {
				completion(false, Result.failure(NetworkError.noConnection.rawValue, statusCode: -995), nil)
				return
			}

			if httpResponse.statusCode == 200 {
				completion(true,nil,nil)
			} else {
				self.handleError(responseData: data, httpResponse, { (result, error) in
					completion(false, result, error)
				})
			}
			
		})
	}
	
	public func registerOAuth(_ email: String, password: String, phoneNumber: String?, userName: String, completion: @escaping (_ success: Bool , _ errorResult: Result<String>?,_ errorBody: ResponseError? ) -> ()) {
		self.router.request(.registerOAuth(email: email, password: password, name: userName, phone: phoneNumber ?? "") , completion: { (data, response, error) in
			if error != nil {
				completion(false, Result.failure(NetworkError.noConnection.rawValue, statusCode: -999), nil)
				return
			}
			guard let httpResponse = response as? HTTPURLResponse else {
				completion(false, Result.failure(NetworkError.noConnection.rawValue, statusCode: -995), nil)
				return
			}
			
			if httpResponse.statusCode == 200 {
				completion(true,nil,nil)
			} else {
				self.handleError(responseData: data, httpResponse, { (result, error) in
					completion(false, result, error)
				})
			}

		})
	}
	
	public func loginOAuth(_ email: String, password: String, completion: @escaping (_ user: UserResponse?, _ errorResult: Result<String>?, _ errorBody: ResponseError? ) -> ()) {
		
		self.router.request(.login(email: email, password: password), completion: { (data, response, error) in
			if error != nil {
				completion(nil, Result.failure(NetworkError.noConnection.rawValue, statusCode: -999), nil)
			}
			
			guard let httpResponse = response as? HTTPURLResponse else {
				completion(nil, Result.failure(NetworkError.noConnection.rawValue, statusCode: -995), nil)
				return
			}
			
			self.handleResponse(responseType: UserResponse.self, data, httpResponse, completion)
		})
	}
	
	public func getCurrentUser(completion: @escaping (_ user: User?, _ errorResult: Result<String>?, _ errorBody: ResponseError? ) -> ()) {
		
		self.router.request(.getUser(ID: 0, accessToken: AccessManager().accessToken()), completion: { (data, response, error) in
		
			if error != nil {
				completion(nil, Result.failure(NetworkError.noConnection.rawValue, statusCode: -999), nil)
			}

			guard let httpResponse = response as? HTTPURLResponse else {
				completion(nil, Result.failure(NetworkError.noConnection.rawValue, statusCode: -995), nil)
				return
			}

			self.handleResponse(responseType: User.self, data, httpResponse, completion)
		})
	}
	
	public func refreshToken(_ completion: @escaping (_ user: UserResponse?, _ errorResult: Result<String>?, _ errorBody: ResponseError? ) -> ()) {
		self.router.request(.refreshToken(accessToken: AccessManager().accessToken()), completion: { (data, response, error) in
			if error != nil {
				completion(nil, Result.failure(NetworkError.noConnection.rawValue, statusCode: -999), nil)
			}
			
			guard let httpResponse = response as? HTTPURLResponse else {
				completion(nil, Result.failure(NetworkError.noConnection.rawValue, statusCode: -995), nil)
				return
			}
			
			self.handleResponse(responseType: UserResponse.self, data, httpResponse, completion)
		})
	}
	
	
}

extension AuthAPIManager {
	
	func handleNetworkResponse(_ response: HTTPURLResponse) -> Result<String> {
		
		switch  response.statusCode {
		case 400			: return .failureResponse
		case 200...299: return .success
		case 401			: return .failure(NetworkResponse.authenticationError.rawValue, statusCode: response.statusCode)
		case 403			: return .failureResponse
		case 500...599: return .failure(NetworkResponse.badRequest.rawValue, statusCode: response.statusCode)
		default:
			return .failure(NetworkResponse.failed.rawValue, statusCode: response.statusCode)
		}
	}
	
	
	func handleResponse<T>(responseType: T.Type ,_ responseData: Data?, _ response: HTTPURLResponse, _ completion: @escaping (T?, Result<String>?, ResponseError?) -> ()) where T : Mappable {

		let result = self.handleNetworkResponse(response)
		switch result {
		case .success :
			guard let responseData = responseData else {
				completion(nil, Result.failure(NetworkResponse.noData.rawValue, statusCode: -998), nil)
				return
			}

			do {
				let apiResponse = try JSONDecoder().decode(T.self, from: responseData)
				completion(apiResponse, nil, nil)
			} catch {
				completion(nil, Result.failure(NetworkResponse.unableToDecode.rawValue, statusCode: -997), nil)
			}
		case .failureResponse:
			handleError(responseData: responseData, response, { (result, error) in
				completion(nil, result, error)
			})
		case .failure(_, _) :
			completion(nil, result, nil)
		}
	}
	
	func handleError(responseData: Data?, _ response: HTTPURLResponse, _ completion: @escaping ( Result<String>?, ResponseError?) -> ()) {
		do {
			let apiResponseError = try JSONDecoder().decode(ResponseError.self, from: responseData!)
			completion(nil, apiResponseError);
		} catch {
			completion(Result.failure(NetworkResponse.unableToDecodeError.rawValue, statusCode: -996), nil)
		}
	}
	
}




