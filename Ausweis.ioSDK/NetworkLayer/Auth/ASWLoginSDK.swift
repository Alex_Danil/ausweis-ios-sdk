//
//  ASWLoginSDK.swift
//  Ausweis.ioSDK
//
//  Created by admin1 on 4/25/18.
//

import Foundation

final public class ASWUserSDK {
	
	private var authManager = AuthAPIManager()
	private var notificationsManager =  NotificationsAPIManager()
	
	public init() {
		
	}
	
	
	public func login(_ email: String, password: String, completion: @escaping (_ user: User?) -> (), failure: @escaping (_ error: ResponseError?) -> ()) {
		authManager.login(email, password: password) { (userResponse, errorResult, errorBody) in
			
			if let user = userResponse?.user {
				AccessManager().setAccessToken(userResponse?.token)
				completion(user)
			} else if let errorBody = errorBody {
				failure(errorBody)
			} else if let errorResult = errorResult {
				if let failureError = errorResult.failureValue() {
					let responseError = ResponseError(statusCode: failureError.code, error: failureError.message)
					failure(responseError)
				}
			}
		}
		
	}
	
	public func refreshToken(_ completion: @escaping (_ success: Bool?) -> (), failure: @escaping (_ error: ResponseError?) -> ()) {
		authManager.refreshToken { (userResponse, errorResult, errorBody) in
						if let token = userResponse?.token {
							AccessManager().setAccessToken(token)
							completion(true)
						} else if let errorBody = errorBody {
							failure(errorBody)
						} else if let errorResult = errorResult {
							if let failureError = errorResult.failureValue() {
								let responseError = ResponseError(statusCode: failureError.code, error: failureError.message)
								failure(responseError)
							}
						}
					}
		
//		login(email, password: password)
		
	}
	
	
	func register(_ email: String, password: String, phoneNumber: String?, userName: String, completion: @escaping (_ success: Bool.Type) -> (), failure: @escaping (_ error: ResponseError?) -> ()) {
		authManager.register(email, password: password, phoneNumber: phoneNumber, userName: userName) { (user, result, response) in
			
		}
	}
	
	public func registerOAuth(_ email: String, password: String, phoneNumber: String?, userName: String, completion: @escaping (_ success: Bool.Type) -> (), failure: @escaping (_ error: ResponseError?) -> ()) {
		
		authManager.registerOAuth(email, password: password, phoneNumber: phoneNumber, userName: userName) { (user, result, response) in
			
		}
	}
	
	public func loginOAuth(_ email: String, password: String, completion: @escaping (_ user: User?) -> (), failure: @escaping (_ error: ResponseError?) -> ()) {
		authManager.loginOAuth(email, password: password) { (userResponse, errorResult, errorBody) in
			
			if let user = userResponse?.user {
				AccessManager().setAccessToken(userResponse?.token)
				completion(user)
			} else if let errorBody = errorBody {
				failure(errorBody)
			} else if let errorResult = errorResult {
				if let failureError = errorResult.failureValue() {
					let responseError = ResponseError(statusCode: failureError.code, error: failureError.message)
					failure(responseError)
				}
			}
		}
		
	}
	
	public func getCurrentUser(_ completion: @escaping (_ user: User?) -> (), failure: @escaping (_ error: ResponseError?) -> ()) {
		authManager.getCurrentUser { (userResponse, errorResult, errorBody) in
			
			if let user = userResponse {
				completion(user)
			} else if let errorBody = errorBody {
				failure(errorBody)
			} else if let errorResult = errorResult {
				if let failureError = errorResult.failureValue() {
					let responseError = ResponseError(statusCode: failureError.code, error: failureError.message)
					failure(responseError)
				}
			}
		}
	}
	
	public func getNotifications(_ offset: Int, limit: Int = 20, completion: @escaping (_ notifications: [Notification]) -> (),  failure: @escaping (_ error: ResponseError?) -> ()) {
		notificationsManager.getNotifications(offset, limit: limit) { (notifications, errorResult, errorBody) in
			if let notifications = notifications {
				completion(notifications.response.result)
			} else if let errorBody = errorBody {
				failure(errorBody)
			} else if let errorResult = errorResult {
				if let failureError = errorResult.failureValue() {
					let responseError = ResponseError(statusCode: failureError.code, error: failureError.message)
					failure(responseError)
				}
			}
		}
		
		
	}

	
	
}
