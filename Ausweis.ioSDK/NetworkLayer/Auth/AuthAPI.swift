//
//  AuthAPI.swift
//  Ausweis.ioSDK
//
//  Created by admin1 on 4/13/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import Foundation

enum AuthAPI {
	case login(email: String, password: String)
	case loginOAuth(email: String, password: String)
	case register(email: String, password: String, name: String, phone: String?)
	case registerOAuth(email: String, password: String, name: String, phone: String?)
	case uploadPhoto(photoPath: String, autorized: Bool, accessToken: String)
	case getUser(ID: Int, accessToken: String)
	case refreshToken( accessToken: String)
}

extension AuthAPI : EndpointType {
	
	var headers: HTTPHeaders? {
		switch self {
		case .login, .register, .refreshToken:
			return nil
		case .uploadPhoto(_,_, let accessToken), .getUser(_, let accessToken):
			return ["Authorization": "JWT \(accessToken)"]
		case .registerOAuth, .loginOAuth:
			return ["content-type": "multipart/form-data"]
		}
	}
	
	var baseURL: URL {
		return URL(string: URLConfiguration.Environment.Develop.rawValue)!
	}
	
	var path: String {
		switch self {
		case .login(_,_),  .uploadPhoto(_, _, _):
			return "/api/v1/auth/api-login-web-token/"
		case .register(_, _,_,_), .getUser:
			return "/api/v1/auth/users/"
		case .registerOAuth(_, _,_,_):
			return "/api/v1/auth/oauth/register/"
		case .loginOAuth(_,_):
			return "/api/v1/auth/oauth/token/"
		case .refreshToken:
			return "/api/v1/auth/api-refresh-web-token/"
		}
		
	}
	
	var httpMethod: HTTPMethod {
		switch self {
 		case .login, .loginOAuth, .register, .registerOAuth, .uploadPhoto , .refreshToken:
			return .post
		case .getUser:
			return .get
		}
	}
	
	var task: HTTPTask {
		switch self {
		case .refreshToken(let accessToken):
			return .requestParameters(bodyParameters: ["token" : accessToken] , urlParameters: nil)
		case .login(let email, let password):
			return .requestParameters(bodyParameters: ["email" : email, "password" : password] , urlParameters: nil)
		case .register(let email, let password, let name, let phone):
			return .requestParameters(bodyParameters: ["email" : email, "password" : password, "name" : name, "phone" : phone ?? ""]
				, urlParameters: nil)
		case .uploadPhoto(let photoPath, _, _):
			return .multipartFromData(filePath: photoPath, bodyParameters: nil, urlParameters: nil)
		case .registerOAuth(let email, let password, let name, let phone):
			let params = ["email" : email,
										"password" : password,
										"name" : name,
										"phone" : phone ?? "",
										"grant_type" : "password",
										"client_id" : "4XWk0C62ufuOBS6OK4kXDaBx91h7U0eLPwTWlKWM",
										"client_secret" : "ijl4UkLeYS7RNG7virzzqU4yBgxNDCOSHeRx5mJhIBctlDs02b1Waa7p9nXary0cUja0DfIR1ltjnyfclb3GNiMJdiBYbKZzlCJg9sKifh6Tedg2mX81sOHGApzVTFna"]
			
			
			return .multipartFromBody(bodyParameters: params , urlParameters: nil, additionalHeaders: headers)
		case .loginOAuth(let email, let password):
			return .multipartFromBody(bodyParameters: ["email": email,
																								 "password": password,
																								 "grant_type" : "password",
																								 "client_id" : "4XWk0C62ufuOBS6OK4kXDaBx91h7U0eLPwTWlKWM",
																								 "client_secret" : "ijl4UkLeYS7RNG7virzzqU4yBgxNDCOSHeRx5mJhIBctlDs02b1Waa7p9nXary0cUja0DfIR1ltjnyfclb3GNiMJdiBYbKZzlCJg9sKifh6Tedg2mX81sOHGApzVTFna"] ,
																urlParameters: nil,
																additionalHeaders: headers)
		case .getUser:
			return .requestParametersAndHeaders(bodyParameters: nil, urlParameters: nil, additionalHeaders: headers)
		}
	}
	
}






