#
#  Be sure to run `pod spec lint Ausweis.ioSDK.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|

  # ―――  Spec Metadata  ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  These will help people to find your library, and whilst it
  #  can feel like a chore to fill in it's definitely to your advantage. The
  #  summary should be tweet-length, and the description more in depth.
  #

  s.name         = "Ausweis.ioSDK"
  s.version      = "0.1.5"
  s.summary      = "Ausweis.io SDK to manage Ausweis API"

  s.homepage     = "https://bitbucket.org/Alex_Danil/ausweis-ios-sdk"
  
  s.license      = { :type => "MIT", :file => "LICENSE.md" }
  s.author       = { "Oleksandr Danylenko" => "sn0wya@me.com" }
 
  s.platform     = :ios
  s.platform     = :ios, "11.0"  

   # 8
  s.source_files = "Ausweis.ioSDK/**/*.{swift,plist}"

  s.source       = { :git => "https://Alex_Danil@bitbucket.org/Alex_Danil/ausweis-ios-sdk.git", :tag => "#{s.version}" ,:branch => "#{s.version}"}

  s.dependency   "Starscream", "~> 3.0.2"




end
